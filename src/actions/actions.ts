
import { action } from "typesafe-actions";

// use typescript enum rather than action constants
export enum actionConst {
    LOGIN = "LOGIN",
    NEWID = "NEWID"
}

export interface ILogin {
    userid: number;
    userName: string;
    token: string;
}

export const appActions = {
    login: (item: ILogin) => action(actionConst.LOGIN, item),
    newID: () => action(actionConst.NEWID)
};
