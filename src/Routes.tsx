// list of the main Routes
import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom';
import Home  from './components/Home';
import About from './components/About';
import Login from './components/Login';
import ProductList from './components/ProductList';
import AddProduct from './components/AddProduct';
import { connect } from "react-redux";
import * as ActionTypes from "ActionTypes";

export class AppRoutes extends Component<any, any> {
    render() {
        if (this.props.userid > 0) {
            return (
                <React.Fragment>
                    <div>userid: {this.props.userid}</div>
                    <Switch>    
                        <Route exact path='/' component={ Home } />
                        <Route path='/login' component={ Login } />
                        <Route path='/products' component={ ProductList } />
                        <Route path='/add' component={ AddProduct } />
                        <Route path='/about' component={ About } />
                    </Switch>
                </React.Fragment>
            )
        } else {
            return <Route component={ Login } />
        }
    }
}

const MapStateToProps = (store: ActionTypes.ReducerState) => {
    return {
        userid: store.state.userid
    };
};

// connect store to App
export default connect(
    MapStateToProps
)(AppRoutes);
