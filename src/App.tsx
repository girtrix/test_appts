import React, { Component } from 'react';
import { Container } from 'reactstrap';
import AppRoutes from './Routes';
import NavigationBar from './components/NavigationBar';

class App extends Component {
  render() {
    return (
        <React.Fragment>
            <NavigationBar />
            <Container>
                <AppRoutes />
            </Container>
        </React.Fragment>
    );
  }
}

export default App;