// src/reducers/index.ts

import * as ActionTypes from "ActionTypes";
import { actionConst } from "../actions/actions";
import { combineReducers } from "redux";

interface IAppState {
    userid: number;
    userName: string;
    token: string;
    lastProdID: number;
}

export const initialState: IAppState = {
    userid: 0,
    userName: "guest",
    token: "",
    lastProdID: 3
};

export const appReducer = (state: IAppState = initialState, action: ActionTypes.RootAction) => {
    switch (action.type) {
        case actionConst.LOGIN: {
            return {
                ...state,
                userid: action.payload.userid,
                userName: action.payload.userName,
                token: action.payload.token
            };
        }
        case actionConst.NEWID: {
            return {
                ...state,
                lastProdID: state.lastProdID + 1
            };
        }
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    state: appReducer //per esporre IAppState come "state" !
});

export default rootReducer;