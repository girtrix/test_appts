import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap';

export default class AddProduct extends Component<any, any> 
{
    constructor(props: any) {
        super(props);
        this.state = { id: props.location.state.lastId + 1, descr: '' };
    }

    handleUpdate = (evt: React.ChangeEvent<HTMLInputElement>) => {
        const target = evt.target;
        const value = target.value;
        const name = target.name;

        //@ts-ignore : workaround to simplify form mgmnt
        this.setState({ [name] : value });
    };

    handleSubmit = (evt: React.FormEvent<HTMLFormElement>) => {
        evt.preventDefault();

        //this.props.location.state.addProduct(this.state);
        // redirect
        this.props.history.push('/products');
    }

    render() {
        return (
            <Container>
                <form style={{marginTop: "20px"}} onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <Row>
                            <Col className="col-md-2">
                                <label htmlFor="title">Codice</label>
                            </Col>
                            <Col className="col-md-2">
                                <div>
                                    <input type="text" className="form-control" name="id" 
                                        value={this.state.id} onChange={this.handleUpdate} readOnly />
                                </div>
                            </Col>
                        </Row>
                    </div>
                                    
                    <div className="form-group">
                        <Row>
                            <Col className="col-md-2">
                                <label htmlFor="description">Descrizione</label>
                            </Col>
                            <Col className="col-md-5">
                                <div>
                                    <input type="text" className="form-control" name="descr" 
                                        value={this.state.descr} onChange={this.handleUpdate} />
                                </div>
                            </Col>
                        </Row>
                    </div>

                    <Row>
                        <div className="form-group row">
                            <div className="col-10">
                                <input className="btn btn-primary" type="submit" value="Submit"  />
                            </div>
                        </div>
                    </Row>
                </form>
            </Container>
        )
    }
}
