import React, { Component } from 'react'
import { Container } from 'reactstrap'
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom'
import AuthService from '../services/authService'
import { connect } from "react-redux";
import { Dispatch } from "redux";
import * as ActionTypes from "ActionTypes";
import { actionConst, ILogin } from "../actions/actions";

interface ILoginProps extends RouteComponentProps<any> {
    userName: string;
    userID: number;
    login: any;
}

export class Login extends Component<ILoginProps, any>
{
    authService: AuthService = new AuthService();
    
    constructor(props: any) {
        super(props);
        this.state = { dummy: 'test' };
    }

    handleSubmit = (evt: React.FormEvent<HTMLFormElement>) => {
        evt.preventDefault();

        let data = { user: 'stub', pwd: 'stub' }
        const service = this.authService.postLogin(data)

        service.then(
            (data: any) => {
                let loginResult: ILogin = { userid: 1, userName: "Gerry", token: "12345"};
                this.props.login(loginResult);

                this.props.history.push('/');
                //return <Redirect to='/' />
            },
            (err: any) => {
                // error handling
            }
        );
    }

    render() {
        return (
            <Container>
                <h2>Login</h2>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group row">
                        <label htmlFor="title">Utente</label>
                        <div className="col-10">
                            <input type="text" className="form-control" name="user" defaultValue={this.props.userName} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="description">Password</label>
                        <div className="col-10">
                            <input type="password" className="form-control" name="password"  />
                        </div>
                    </div>

                    <div className="form-group row">
                        <div className="col-10">
                            <input className="btn btn-primary" type="submit" value="Submit"  />
                        </div>
                    </div>

                    <div>user id: {this.props.userID} user name: {this.props.userName}</div>
                </form>
            </Container>
        )
    }
}

const MapStateToProps = (store: ActionTypes.ReducerState) => {
    return {
        userName: store.state.userName,
        userID: store.state.userid
    };
};

const MapDispatchToProps = (dispatch: Dispatch<ActionTypes.RootAction>) => ({
    login: (item: ILogin) => dispatch({ type: actionConst.LOGIN, payload: item })
});

export default connect(
    MapStateToProps,
    MapDispatchToProps
)(Login);

/*
export default withRouter( connect(
    MapStateToProps,
    MapDispatchToProps
)(Login));
*/
