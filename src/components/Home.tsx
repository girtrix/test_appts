import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap';
import * as ActionTypes from "ActionTypes";
import { connect } from "react-redux";
//import { withRouter } from 'react-router-dom';

export class Home extends Component<any, any> {
    render() {
        return (
            <Container>
                <Row>
                    <Col className="col-12 text-center">
                        <h2>Welcome { this.props.userName }</h2>
                    </Col>
                </Row>
            </Container>
        )
    }
}

const MapStateToProps = (store: ActionTypes.ReducerState) => {
    return {
        userName: store.state.userName
    };
};

// connect store to App
export default connect(
    MapStateToProps
)(Home);
