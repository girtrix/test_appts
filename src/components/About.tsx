import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap';

export default class About extends Component {
    render() {
        return (
            <Container>
                <Row>
                    <Col className="col-12 text-center">
                        <h2>About the application ...</h2>
                    </Col>
                </Row>
                <Row>
                    <Col className="col-12 text-center">
                        Powered by <img src="images/logo.png" />
                    </Col>
                </Row>
            </Container>
        )
    }
}
