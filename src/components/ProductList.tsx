import React, { Component } from 'react'
import ProductDataService from '../services/dataService'
import { Container, Row, Button, Col, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
//import { Link } from 'react-router-dom';
import * as ActionTypes from "ActionTypes";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { actionConst } from "../actions/actions";

export class ProductList extends Component<any, any> 
{
    dataService: ProductDataService = new ProductDataService();

    constructor(props: any) {
        super(props);
        this.state = { error: null, products: [], loading: true, modal: false, action: '' };
    }

    componentDidMount() {
        this.dataService
            .getProducts()
            .then(
                (data: any) => {
                    data = [{id: 1, descr: 'Prodotto 1'}, {id: 2, descr: 'un altro prodotto'}, {id: 3, descr: 'Il terzo prodotto'}];

                    this.setState(
                        { loading: false, products: data, descrProd: '', curId: 0 }
                    );
                },
                (err: any) => {
                    this.setState(
                        { loading: false, error: err }
                    );
                }
            );
    }

    handleUpdate = (evt: React.ChangeEvent<HTMLInputElement>) => {
        const target = evt.target;
        const value = target.value;
        const name = target.name;

        //@ts-ignore : workaround to simplify form mgmnt
        this.setState({ [name] : value });
    };

    saveProduct = () => {
        if (this.state.action == 'new') {
            const product = {id: this.props.lastId + 1, descr: this.state.descrProd}
            this.props.newId();
            this.setState({ products: this.state.products.concat(product), modal: false })
        } else {
            const products = [...this.state.products].filter((prod) => { 
                if (prod.id == this.state.curId) {
                    prod.descr = this.state.descrProd;
                }
                return true;
            })

            this.setState({products: products, modal: false});
        }
    }

    deleteProduct = (id: number) => {
        const products = [...this.state.products];

        this.setState({products: products.filter(
            function(prod) { 
                return prod.id !== id;
            })
        });
    }

    openAdd = () => {
        this.setState({ action: 'new', modal: !this.state.modal  });
    }

    openEdit = (id: number, descr: string) => {
        this.setState({ action: 'edit', curId: id, descrProd: descr, modal: !this.state.modal });
    }

    closeEdit = () => {
        this.setState({ modal: !this.state.modal });
    }

    render() {
        const { loading, error, products } = this.state;

        if (loading)
            return <div>Loading ...</div>;

        const listOfProducts = products.map(
            (prod: any) => <tr key={prod.id}>
                    <td>{prod.id}</td>
                    <td>{prod.descr}</td>
                    {/*<td><Link className="btn btn-info" to={"/edit/" + prod.id}>edit</Link></td>*/}
                    <td><Button className="btn btn-info" onClick={() => this.openEdit(prod.id, prod.descr)}>edit</Button></td>
                    <td><Button className="btn btn-warning" onClick={() => this.deleteProduct(prod.id)}>delete</Button></td>
                </tr>
        );

        return (
            <Container>
                <Row>
                    <Col sm="12">
                        <Button color="primary" onClick={this.openAdd}>Aggiungi Prodotto</Button>
                        {/*
                        <Link className="btn btn-primary" to={{
                                pathname: '/add',
                                state: {
                                    lastId: this.props.lastId
                                }
                        }}>
                            Aggiungi Prodotto
                        </Link>
                        */}
                    </Col>
                </Row>
                <Row>
                    <Col sm="12" style={{marginTop: "10px"}}>
                        <Table striped>
                            <thead>
                                <tr>
                                    <th>Codice</th>
                                    <th>Descrizione</th>
                                </tr>
                            </thead>
                            <tbody>
                                {listOfProducts}
                            </tbody>
                        </Table>
                    </Col>
                </Row>

                <Modal isOpen={this.state.modal} fade={false} toggle={this.closeEdit} className={this.props.className}>
                    <ModalHeader toggle={this.closeEdit}>{(this.state.action == 'new') ? 'Nuovo Prodotto' : 'Modifica Prodotto'}</ModalHeader>
                    <ModalBody>
                        <div className="form-group">
                            <Row>
                                <Col className="col-md-2">
                                    <label htmlFor="title">Codice</label>
                                </Col>
                                <Col className="col-md-2">
                                    <div>
                                        <input type="text" className="form-control" name="id" 
                                            value={(this.state.action == 'new') ? this.props.lastId + 1 : this.state.curId} onChange={this.handleUpdate} readOnly />
                                    </div>
                                </Col>
                            </Row>
                        </div>

                        <div className="form-group">
                            <Row>
                                <Col className="col-md-2">
                                    <label htmlFor="description">Descrizione</label>
                                </Col>
                                <Col className="col-md-5">
                                    <div>
                                        <input type="text" className="form-control" name="descrProd" 
                                            value={this.state.descrProd} onChange={this.handleUpdate} />
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button color='primary' onClick={this.saveProduct}>Salva</Button>
                        <Button color='secondary' onClick={this.closeEdit}>Annulla</Button>
                    </ModalFooter>
                </Modal>
            </Container>
        )
    }
}

const MapStateToProps = (store: ActionTypes.ReducerState) => {
    return {
        lastId: store.state.lastProdID
    };
};

const MapDispatchToProps = (dispatch: Dispatch<ActionTypes.RootAction>) => ({
    newId: () => dispatch({ type: actionConst.NEWID })
});

// connect store to App
export default connect(
    MapStateToProps,
    MapDispatchToProps
)(ProductList);