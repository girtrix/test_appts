
const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export default class ProductDataService {
    private url: string = 'https://qualche_url';

    private basicHeader: Headers = new Headers({
        'Accept': 'application/json',
        'Content-Type':  'application/json'
    });

    async getProducts() {
        /*
        const promise = await fetch(this.url,{
            method: 'GET',
            body: JSON.stringify(data),
            headers: this.basicHeader
        });
        */

        const promise = sleep(500)
        return promise;
    }
}
