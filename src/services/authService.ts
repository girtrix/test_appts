
const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export interface IUserLogin {
    user: string;
    pwd: string;
}

export default class AuthService {
    private url: string = 'https://qualche_url';

    private basicHeader: Headers = new Headers({
        'Accept': 'application/json',
        'Content-Type':  'application/json'
    });

    async postLogin(data: IUserLogin) {
        /*
        const promise = await fetch(this.url,{
            method: 'POST',
            body: JSON.stringify(data),
            headers: this.basicHeader
        });
        */

        const promise = sleep(600)
        return promise;
    }
}
